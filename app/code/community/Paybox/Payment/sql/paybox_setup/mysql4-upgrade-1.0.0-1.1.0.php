<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Paybox
 * @package     Paybox_Payment
 * @author      Netapsys Magento Team <magento@netapsys.fr>
 */

$installer = $this;

/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer->startSetup();

$installer->run("
	INSERT INTO `{$installer->getTable('core_config_data')}`
	(scope, scope_id, path, value)
	SELECT scope, scope_id, 'paybox/account/cgi_file_path', value
	FROM `{$installer->getTable('core_config_data')}`
	WHERE path='payment/paybox_system/cgi_file_path';
	
	DELETE FROM `{$installer->getTable('core_config_data')}`
	WHERE path='payment/paybox_system/cgi_file_path';
");

$installer->endSetup();