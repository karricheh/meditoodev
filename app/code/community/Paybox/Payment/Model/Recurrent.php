<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Paybox
 * @package     Paybox_Payment
 * @copyright   Copyright (c) 2011 Netapsys Conseil (http://www.netapsys.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Paybox System Recurrent module
 *
 * @category    Paybox
 * @package     Paybox_Payment
 * @author      Netapsys Magento Team <magento@netapsys.fr>
 */
class Paybox_Payment_Model_Recurrent extends Paybox_Payment_Model_System
{

    protected $_code = 'paybox_recurrent';
    protected $_formBlockType = 'paybox/recurrent_form';
    protected $_infoBlockType = 'paybox/recurrent_info';

    /**
     * Returl redirect URL to checkout
     *
     * @return string
     */
    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('paybox/recurrent/redirect', array('_secure' => true));
    }

    /**
     * Building array of params to send
     *
     * @param string $_paymentMethod
     * @return array
     * @codeCoverageIgnore
     */
    public function getFormFields($_paymentMethod = 'commandline')
    {
        $fieldsArr = parent::getFormFields($_paymentMethod);
        $debitsCount = (int) $this->getConfigData('ibs_nbpaie');
        $baseGrandTotal = $this->getOrder()->getBaseGrandTotal();

        if ($this->getConfigData('include_shipping')) {
            $debitPerMonth = (int) (($baseGrandTotal / $debitsCount) * 100);
            $pbxTotal = (int) ($baseGrandTotal * 100) - ($debitPerMonth * ($debitsCount - 1));
        } else {
            $debitPerMonth = (int) ((($baseGrandTotal - $this->getOrder()->getShippingAmount()) / $debitsCount) * 100);
            $pbxTotal = (int) ($baseGrandTotal * 100) - ($debitPerMonth * ($debitsCount - 1));
        }

        $fieldsArr['PBX_TOTAL'] = $pbxTotal;
        $fieldsArr['PBX_CMD'] .= 'IBS_2MONT' . str_pad($debitPerMonth, 10, "0", STR_PAD_LEFT) .
                'IBS_NBPAIE' . str_pad($debitsCount - 1, 2, "0", STR_PAD_LEFT) .
                'IBS_FREQ' . str_pad($this->getConfigData('ibs_freq'), 2, "0", STR_PAD_LEFT) .
                'IBS_QUAND' . str_pad($this->getConfigData('ibs_quand'), 2, "0", STR_PAD_LEFT);

        if ($this->getConfigData('ibs_delais') && $this->getConfigData('ibs_delais') != "")
            $fieldsArr['PBX_CMD'] .= 'IBS_DELAIS' . str_pad($this->getConfigData('ibs_delais'), 3, "0", STR_PAD_LEFT);

        return $fieldsArr;
    }

    /**
     * IPN for Instant Payment Notification
     * when Paybox server call Magento to validate (no not) payment
     *
     * @param array $params
     * @return void
     * @codeCoverageIgnore
     */
    public function processIpnRequest($params)
    {
        $params['nb_paie'] = $this->_getNbPaieFromRef($params['ref']);
        $params['ref'] = $this->_getIncrementIdFromRef($params['ref']);

        if (isset($params['ETAT_PBX']) && $params['ETAT_PBX'] == 'PBX_RECONDUCTION_ABT') {
            $this->processDebitIpnRequest($params);
        } else {
            parent::processIpnRequest($params);
        }
    }

    /**
     * Debit IPN for Debit Instant Payment Notification
     * when Paybox server call Magento for each subscription debit
     *
     * @param array $params
     * @return void
     * @codeCoverageIgnore
     */
    public function processDebitIpnRequest($params)
    {
        try {
            $order = Mage::getModel('sales/order')
                    ->loadByIncrementId($params['ref']);

            $this->_checkPayboxParams($order, $params, 'ipn');

            if ($params['error'] == '00000') {
                // Payment accepted by Paybox
                // State and status Processing
//                $order->setState(
//                        Mage_Sales_Model_Order::STATE_PROCESSING,
//                        Mage_Sales_Model_Order::STATE_PROCESSING,
//                        Mage::helper('paybox')->__('Payment accepted by Paybox')
//                );
                // Save transaction                
                $payment = $order->getPayment();
                $payment->setTransactionId($params['trans']);
                $transaction = $payment->addTransaction(
                        Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE, null, false, 'Paybox : '
                );
                $transaction->setAdditionalInformation(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS, $params);
                $transaction->setIsClosed(true);

                $payment->setData('cc_trans_id', $params['trans']);

                // Billing creation
//                if ($this->_createInvoice($order, $params)) {
//                    $order->addStatusToHistory($order->getStatus(), Mage::helper('paybox')->__('Invoice successfully created'));
//                } else {
//                    $order->addStatusToHistory($order->getStatus(), Mage::helper('paybox')->__('Can\'t create invoice'));
//                }
                $order->addStatusToHistory($order->getStatus(), Mage::helper('paybox')->__('Monthly debit of %s captured by Paybox', Mage::helper('core')->formatPrice($params['amount'] / 100, false)));

                // Email notification
//                if (!$order->getEmailSent()) {
//                    $order->sendNewOrderEmail();
//                }
            } else {
                // Payment refused by Paybox
                $errorMsg = Mage::helper('paybox')->__('Monthly debit of %s refused by Paybox');
                $errorMsg .= ' - ';
                $errorMsg .= Mage::helper('paybox')->__('Error code: %s', $params['error']);

                // Order cancel
                $order->addStatusToHistory(
                        Mage_Sales_Model_Order::STATE_HOLDED, $errorMsg
                );
                $order->hold();
            }

            $order->save();
        } catch (Exception $e) {
            $order->addStatusToHistory(
                    $order->getStatus(), Mage::helper('paybox')->__('Error in monthly debit validation : %s', $e->getMessage())
            )->save();
        }
    }

    /**
     * When customer return on Magento after payment accepted
     *
     * @param array $params
     * @return void
     * @codeCoverageIgnore
     */
    public function processSuccessRequest($params)
    {
        $params['ref'] = $this->_getIncrementIdFromRef($params['ref']);
        parent::processSuccessRequest($params);
    }

    /**
     * When customer return on Magento after payment refused
     *
     * @param array $params
     * @return string $errMsg
     * @codeCoverageIgnore
     */
    public function processRefuseRequest($params)
    {
        $params['ref'] = $this->_getIncrementIdFromRef($params['ref']);
        return parent::processRefuseRequest($params);
    }

    /**
     * When customer return on Magento without paying
     *
     * @param array $params
     * @return void
     * @codeCoverageIgnore
     */
    public function processDeclineRequest($params)
    {
        $params['ref'] = $this->_getIncrementIdFromRef($params['ref']);
        parent::processDeclineRequest($params);
    }

    /**
     * Check params from Paybox
     *
     * @param Mage_Sales_Model_Order $order
     * @param array $params
     * @param string $action
     * @return bool
     * @codeCoverageIgnore
     */
    protected function _checkPayboxParams($order, $params, $action = null)
    {
        try {
            parent::_checkPayboxParams($order, $params, $action);
        } catch (Exception $e) {
            // Don't check amount in recurrent payment
            if ($e->getMessage() == Mage::helper('paybox')->__("Amount does'nt match")) {
                return true;
            } else {
                Mage::throwException($e->getMessage());
            }
        }
    }

    /**
     * Retrieve payment method title integrating number of payments
     *  
     * @return string 
     */
    public function getTitle()
    {
        return sprintf(parent::getTitle(), $this->getConfigData('ibs_nbpaie'));
    }

    /**
     * Retrieve payment method title integrating number of payments
     *  
     * @return string 
     */
    public function getTitleWithNbPaie()
    {
        return sprintf(parent::getTitle(), $this->getInfoInstance()->getPayboxNbPaie());
    }

    /**
     * Extract IncrementId from Paybox $params['ref']
     * 
     * @param string $ref
     * @return string
     * @codeCoverageIgnore
     */
    protected function _getIncrementIdFromRef($ref)
    {
        return substr($ref, 0, strpos($ref, 'IBS_2MONT'));
    }

    /**
     * Extract NbPaie from Paybox $params['ref']
     * 
     * @param string $ref
     * @return string
     * @codeCoverageIgnore
     */
    protected function _getNbPaieFromRef($ref)
    {
        return (int) substr($ref, strpos($ref, 'IBS_NBPAIE') + 10, 2);
    }

}
