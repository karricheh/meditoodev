<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Paybox
 * @package     Paybox_Payment
 * @copyright   Copyright (c) 2011 Netapsys Conseil (http://www.netapsys.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Paybox Fees on Quote Totals
 *
 * @deprecated
 * @category    Paybox
 * @package     Paybox_Payment
 * @author      Netapsys Magento Team <magento@netapsys.fr>
 */
//class Paybox_Payment_Model_Quote_Address_Total_Paybox extends Mage_Sales_Model_Quote_Address_Total_Abstract
//{
//    /**
//     * Constructor class
//     * 
//     * @return void 
//     */
//    public function __construct()
//    {
//        $this->setCode('paymentFee');
//    }
//
//    /**
//     * Collect Paybox payment fee
//     * 
//     * @param Mage_Sales_Model_Quote_Address $address
//     * @return Paybox_Payment_Model_Quote_Address_Total_Paybox 
//     */
//    public function collect(Mage_Sales_Model_Quote_Address $address)
//    {
//        parent::collect($address);
//
//        // ... Some your api calls to retrive amount ...
//        $calculatedAmount = 55;
//        $address->setPaymentFee($calculatedAmount);
//
//        // Set base amount of your custom fee
//        $this->_setBaseAmount($calculatedAmount);
//
//        // Set amount of your custom fee in displayed currency
//        $this->_setAmount(
//            $address->getQuote()->getStore()->convertPrice($calculatedAmount, false)
//        );
//
//        return $this;
//        
//    }
//
//    /**
//     * Add (or not) Paybox fee amount in quote totals
//     * 
//     * @param Mage_Sales_Model_Quote_Address $address 
//     * @return void
//     */
//    public function fetch(Mage_Sales_Model_Quote_Address $address)
//    {
//        // Display total only if it is not zero
//        if ($address->getPaymentFee() != 0) {
//            $address->addTotal(array(
//                'code' => $this->getCode(),
//                'title' => 'My Custom Duty',
//                'value' => $address->getPaymentFee()
//            ));
//        }
//    }
//    
//}
