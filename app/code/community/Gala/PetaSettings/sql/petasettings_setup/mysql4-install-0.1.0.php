<?php

$installer = $this;

$installer->startSetup();

/*
$installer->addAttribute('catalog_product', 'EM_Featured', array(
        'group'             => 'General',
        'type'              => 'int',
        'backend'           => '',
        'frontend'          => '',
        'label'             => 'Featured Product',
        'input'             => 'boolean',
        'class'             => '',
        'source'            => '',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'           => true,
        'required'          => false,
        'user_defined'      => true,
        'default'           => '0',
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => false,
        'unique'            => false,
        'apply_to'          => 'simple,configurable,virtual,bundle,downloadable',
        'is_configurable'   => false
    ));
*/
$block = Mage::getModel('cms/block');
$page = Mage::getModel('cms/page');
//$stores = array_keys(Mage::getSingleton('adminhtml/system_store')->getStoreOptionHash());

$stores = array(0);

// Config perfix for identifier of static block and static page
$prefixBlock = 'galapeta_';
$prefixPage = 'galapeta_';


####################################################################################################
# INSERT STATIC BLOCKS
####################################################################################################

/*$dataBlock = array(
	'title' => 'aaaa',
	'identifier' => $prefixBlock.'aaaa',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
		aaaa
EOB
);
$block->setData($dataBlock)->save();*/

$dataBlock = array(
	'title' => 'Footer Links',
	'identifier' => $prefixBlock.'footer_links',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
		<ul>
<li><a href="{{store direct_url="about-magento-demo-store"}}">About Us</a></li>
<li class="last"><a href="{{store direct_url="customer-service"}}">Customer Service</a></li>
</ul>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Main Slideshow',
	'identifier' => $prefixBlock.'main_slideshow',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
		<div>{{widget type="slideshowwidget/create" url1="reptiles.html" image1="1.jpg" url2="cats.html" image2="2.jpg" url3="dogs.html" image3="3.jpg" width="960" height="273" delay="5000" displaybutton="true" autostart="true" transition="random" transitionspeed="800" autocenter="false" cpanelalign="BR" cpanelposition="inside" timeralign="top" displaytimer="true" mouseoverpause="false" cpanelmouseover="false" textmouseover="false" texteffect="fade" textsync="true" shuffle="false"}}</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Slideshow Top',
	'identifier' => $prefixBlock.'slideshow_top',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
		<ul>
<li class="item first"><a href="#"><img src="{{skin url="images/slideshow-top/1.png"}}" alt="" /></a></li>
<li class="item"><a href="#"><img src="{{skin url="images/slideshow-top/2.png"}}" alt="" /></a></li>
<li class="item last"><a href="#"><img src="{{skin url="images/slideshow-top/3.png"}}" alt="" /></a></li>
</ul>
EOB
);
$block->setData($dataBlock)->save();


$dataBlock = array(
	'title' => 'Slideshow Bottom',
	'identifier' => $prefixBlock.'slideshow_bottom',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
				<ul>
<li class="item first"><a href="#"><img src="{{skin url="images/slideshow-fotter/1.png"}}" alt="" /></a></li>
<li class="item"><a href="#"><img src="{{skin url="images/slideshow-fotter/2.png"}}" alt="" /></a></li>
<li class="item"><a href="#"><img src="{{skin url="images/slideshow-fotter/3.png"}}" alt="" /></a></li>
<li class="item"><a href="#"><img src="{{skin url="images/slideshow-fotter/4.png"}}" alt="" /></a></li>
<li class="item"><a href="#"><img src="{{skin url="images/slideshow-fotter/1.png"}}" alt="" /></a></li>
<li class="item"><a href="#"><img src="{{skin url="images/slideshow-fotter/3.png"}}" alt="" /></a></li>
<li class="item"><a href="#"><img src="{{skin url="images/slideshow-fotter/4.png"}}" alt="" /></a></li>
<li class="item last"><a href="#"><img src="{{skin url="images/slideshow-fotter/5.png"}}" alt="" /></a></li>
</ul>

EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Fotter follow us on',
	'identifier' => $prefixBlock.'footer_1',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
		<div class="content">
<ul>
<li class="item first"><a title="Facebook" href="#"><img src="{{skin url="images/follow-us-on/facebook.png"}}" alt="Facebook" /></a></li>
<li class="item"><a title="pick on" href="#"><img src="{{skin url="images/follow-us-on/pick-on.png"}}" alt="pick on" /></a></li>
<li class="item"><a title="twitter" href="#"><img src="{{skin url="images/follow-us-on/twitter.png"}}" alt="twitter" /></a></li>
<li class="item last"><a title="RSS" href="#"><img src="{{skin url="images/follow-us-on/rss.png"}}" alt="RSS" /></a></li>
</ul>
</div>

EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Footer We Accept',
	'identifier' => $prefixBlock.'footer_2',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
		<div class="content">
<ul>
<li class="first"><a title="Visa" href="#"><img src="{{skin url="images/we_accept/image-visa.jpg"}}" alt="Visa" /></a></li>
<li><a title="Master Cart" href="#"><img src="{{skin url="images/we_accept/image-cart.jpg"}}" alt="Master Cart" /></a></li>
<li><a title="American Cart" href="#"><img src="{{skin url="images/we_accept/image-cart1.jpg"}}" alt="American Cart" /></a></li>
<li class="last"><a title="Paypal" href="#"><img src="{{skin url="images/we_accept/image-paypal.jpg"}}" alt="Paypal" /></a></li>
</ul>
</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Menu Top',
	'identifier' => $prefixBlock.'menu_top',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
			<ul class="menu">
<!-- BEGIN MENU -->
<li class="submenu last"><a class="drop level-top" href="{{store direct_url=''}}"><span>HOME</span></a></li>
<li class="submenu"><a class="drop level-top" href="{{store direct_url='dogs.html'}}"><span>DOGS</span></a>
<div class="dropdown_1columns">
<div class="inner">{{widget type="megamenu/catalogmenu" category="2" template="em_megamenu/menu.phtml" set_layout_menu="3" max_row_per_column="10"}}</div>
</div>
</li>
<li class="submenu"><a class="drop level-top" href="{{store direct_url='cats.html'}}"><span>CATS</span></a>
<div class="dropdown_6columns">
<div class="inner"><span class="title">CATS</span>
<div class="col_6 firstcolumn label">
<div class="col_1 no-icon first"><a title="Pedigree" href="#"><img src="{{skin url='images/slideshow-fotter/1.png'}}" alt="Pedigree" /></a></div>
<div class="col_1 no-icon"><a title="Nutro" href="#"><img src="{{skin url='images/slideshow-fotter/2.png'}}" alt="Nutro" /></a></div>
<div class="col_1 no-icon"><a title="Whiskas" href="#"><img src="{{skin url='images/slideshow-fotter/3.png'}}" alt="Whiskas" /></a></div>
<div class="col_1 no-icon"><a title="Wellness" href="#"><img src="{{skin url='images/slideshow-fotter/4.png'}}" alt="Wellness" /></a></div>
<div class="col_1 no-icon "><a title="FancyFeast" href="#"><img src="{{skin url='images/slideshow-fotter/5.png'}}" alt="FancyFeast" /></a></div>
<div class="col_1 no-icon last"><a title="Pedigree" href="#"><img src="{{skin url='images/slideshow-fotter/1.png'}}" alt="Pedigree" /></a></div>
</div>
</div>
</div>
<!-- End6 column container --> </li>
<li class="submenu "><a class="drop level-top" href="{{store direct_url='reptiles.html'}}"><span>REPTILES </span> </a>
<div class="dropdown_2columns">
<div class="inner">
<div class="col_2 firstcolumn wrapper_col">
<div class="col_1"><span class="title_col">Lorem Ipsum </span> 
<ul>
<li class="level1 nav-3-1 first"><a href="{{store direct_url='cats/foods.html'}}">Phasellus Purus</a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='reptiles/foods.html'}}">Laoreet Sed</a></li>
<li class="level1 nav-3-3"><a href="{{store direct_url='reptiles/apparel.html'}}">Nulla Quam </a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/apparel.html'}}">Morbi Odio</a></li>
<li class="level1 nav-3-5"><a href="{{store direct_url='cats/apparel.html'}}">Amet Bibendum </a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/apparel.html'}}">Tristique 		Turpis</a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/foods.html'}}">Amet Bibendum</a></li>
</ul>
</div>
<div class="col_1"><span class="title_col">Vulputate</span> 
<ul>
<li class="level1 nav-3-1 first"><a href="{{store direct_url='cats/foods.html'}}">Phasellus Purus</a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/foods.html'}}">Laoreet Sed</a></li>
<li class="level1 nav-3-3"><a href="{{store direct_url='cats/foods.html'}}">Nulla Quam </a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/apparel.html'}}">Morbi Odio</a></li>
<li class="level1 nav-3-5"><a href="{{store direct_url='cats/apparel.html'}}">Amet Bibendum </a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/apparel.html'}}">Tristique 		Turpis</a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/foods.html'}}">Amet Bibendum</a></li>
</ul>
</div>
</div>
<div class="col_2 wrapper_col">
<div class="col_1"><span class="title_col">Lorem Ipsum </span> 
<ul>
<li class="level1 nav-3-1 first"><a href="{{store direct_url='cats/foods.html'}}">Phasellus Purus</a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/foods.html'}}">Laoreet Sed</a></li>
<li class="level1 nav-3-3"><a href="{{store direct_url='cats/foods.html'}}">Nulla Quam </a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/apparel.html'}}">Morbi Odio</a></li>
<li class="level1 nav-3-5"><a href="{{store direct_url='cats/apparel.html'}}">Amet Bibendum </a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/apparel.html'}}">Tristique 		Turpis</a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/foods.html'}}">Amet Bibendum</a></li>
</ul>
</div>
<div class="col_1"><span class="title_col">Vulputate</span> 
<ul>
<li class="level1 nav-3-1 first"><a href="{{store direct_url='cats/foods.html'}}">Phasellus Purus</a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/foods.html'}}">Laoreet Sed</a></li>
<li class="level1 nav-3-3"><a href="{{store direct_url='cats/foods.html'}}">Nulla Quam </a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/apparel.html'}}">Morbi Odio</a></li>
<li class="level1 nav-3-5"><a href="{{store direct_url='cats/apparel.html'}}">Amet Bibendum </a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/apparel.html'}}">Tristique 		Turpis</a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/foods.html'}}">Amet Bibendum</a></li>
</ul>
</div>
</div>
</div>
</div>
<!-- End2 column container --></li>
<li class="submenu "><a class="drop level-top" href="{{store url='reptiles.html'}}"><span>SMALL PETS</span></a>
<div class="dropdown_6columns ">
<div class="inner">
<div class="col_6">
<div class="col_2 firstcolumn"><a href="#"><img src="{{skin url='images/media/menu/image-product2.jpg'}}" alt="" width="100" height="100" /></a>
<p>Aliquam tristique faucibus metus, nec malesuada bibe libero viverra at. Quisque ornare neque est. Nulla leon sapien, placerat ac fringilla sit amet, sagittisn tincidunt enim malesuada lectus. Sed a orci ligula. Maecetortor urna aliquet dignissim maecenas dolor eros.</p>
<p>Fusce dignissim, justo quis aliquam imperdiet, elit urna porttitor odio, vitae imperdiet justo purus id tellus. Nulla facilisi. Aliquam auctor molestie madictum cidunt rhoncus. Quisque diam sapien, viverra ut tincidunt vitae, tempor ac leo. Donec at pharetra velit. Curabitur porttitor venenatis nisi, sit amet accumsan tellus urna laoreet non. Vestibulum ac tincidunt enim. Nullam leo metus, dictum consectetur pulvinar sed, tincidunt ut urna. Cras vel dui velit, pretium lacinia. Pellentesque massa dolor, fermentum consectetur</p>
</div>
<div class="col_1"><span class="title_col">Sem Eleifend </span> 
<ul>
<li class="level1 nav-3-1 first"><a href="{{store direct_url='cats/foods.html'}}">Phasellus Purus</a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/foods.html'}}">Laoreet Sed</a></li>
<li class="level1 nav-3-3"><a href="{{store direct_url='cats/foods.html'}}">Nulla Quam </a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/apparel.html'}}">Morbi Odio</a></li>
<li class="level1 nav-3-5"><a href="{{store direct_url='cats/apparel.html'}}">Amet Bibendum </a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/apparel.html'}}">Tristique 		Turpis</a></li>
<li class="level1 nav-3-3"><a href="{{store direct_url='cats/foods.html'}}">Phasellus leo</a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/foods.html'}}">Amet Bibendum</a></li>
<li class="level1 nav-3-5"><a href="{{store direct_url='cats/foods.html'}}">Tristique Turpis</a></li>
</ul>
</div>
<div class="col_1"><span class="title_col">Lobortis Nunc </span> 
<ul>
<li class="level1 nav-3-1 first"><a href="{{store direct_url='cats/foods.html'}}">Phasellus Purus</a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/foods.html'}}">Laoreet Sed</a></li>
<li class="level1 nav-3-3"><a href="{{store direct_url='cats/foods.html'}}">Nulla Quam </a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/apparel.html'}}">Morbi Odio</a></li>
<li class="level1 nav-3-5"><a href="{{store direct_url='cats/apparel.html'}}">Amet Bibendum </a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/apparel.html'}}">Tristique 		Turpis</a></li>
<li class="level1 nav-3-3"><a href="{{store direct_url='cats/foods.html'}}">Phasellus leo</a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/foods.html'}}">Amet Bibendum</a></li>
<li class="level1 nav-3-5"><a href="{{store direct_url='cats/foods.html'}}">Tristique Turpis</a></li>
</ul>
</div>
<div class="col_1"><span class="title_col">Sem Eleifend </span> 
<ul>
<li class="level1 nav-3-1 first"><a href="{{store direct_url='cats/foods.html'}}">Phasellus Purus</a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/foods.html'}}">Laoreet Sed</a></li>
<li class="level1 nav-3-3"><a href="{{store direct_url='cats/foods.html'}}">Nulla Quam </a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/apparel.html'}}">Morbi Odio</a></li>
<li class="level1 nav-3-5"><a href="{{store direct_url='cats/apparel.html'}}">Amet Bibendum </a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/apparel.html'}}">Tristique 		Turpis</a></li>
<li class="level1 nav-3-3"><a href="{{store direct_url='cats/foods.html'}}">Phasellus leo</a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/foods.html'}}">Amet Bibendum</a></li>
<li class="level1 nav-3-5"><a href="{{store direct_url='cats/foods.html'}}">Tristique Turpis</a></li>
</ul>
</div>
<div class="col_1 most_popular"><span class="title_col"> TOP FAVORITE </span>
<div class="product">{{widget type="flexiblewidget/list" column_count="4" limit_count="9" order_by="name asc" template="custom_template" custom_theme="flexiblewidget/most_popular_menu.phtml"}}</div>
</div>
</div>
</div>
</div>
<!-- End 3 columns container --></li>
<li class="submenu"><a class="drop level-top" href="{{store direct_url='cats.html'}}"><span>BEST SELLER </span></a>
<div class="dropdown_6columns ">
<div class="inner"><span class="title">women</span>
<p class="note first">This is an example of a large contaner with 6 columns</p>
<div class="col_6 firstcolumn">
<div class="col_1 firstcolumn"><span class="title_col">LobortisLputate</span> 
<ul>
<li class="level1 nav-3-1 first"><a href="{{store direct_url='cats/foods.html'}}">Phasellus Purus</a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/foods.html'}}">Laoreet Sed</a></li>
<li class="level1 nav-3-3"><a href="{{store direct_url='cats/foods.html'}}">Nulla Quam </a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/apparel.html'}}">Morbi Odio</a></li>
</ul>
</div>
<div class="col_1"><span class="title_col">Malesuada </span> 
<ul>
<li class="level1 nav-3-1 first"><a href="{{store direct_url='cats/foods.html'}}">Phasellus Purus</a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/foods.html'}}">Laoreet Sed</a></li>
<li class="level1 nav-3-3"><a href="{{store direct_url='cats/foods.html'}}">Nulla Quam </a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/apparel.html'}}">Morbi Odio</a></li>
</ul>
</div>
<div class="col_1"><span class="title_col">Lobortis Vulpu</span> 
<ul>
<li class="level1 nav-3-1 first"><a href="{{store direct_url='cats/foods.html'}}">Phasellus Purus</a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/foods.html'}}">Laoreet Sed</a></li>
<li class="level1 nav-3-3"><a href="{{store direct_url='cats/foods.html'}}">Nulla Quam </a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/apparel.html'}}">Morbi Odio</a></li>
</ul>
</div>
<div class="col_1"><span class="title_col">Vulputate</span> 
<ul>
<li class="level1 nav-3-1 first"><a href="{{store direct_url='cats/foods.html'}}">Phasellus Purus</a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/foods.html'}}">Laoreet Sed</a></li>
<li class="level1 nav-3-3"><a href="{{store direct_url='cats/foods.html'}}">Nulla Quam </a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/apparel.html'}}">Morbi Odio</a></li>
</ul>
</div>
<div class="col_1"><span class="title_col">Phasellus lec</span> 
<ul>
<li class="level1 nav-3-1 first"><a href="{{store direct_url='cats/foods.html'}}">Phasellus Purus</a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/foods.html'}}">Laoreet Sed</a></li>
<li class="level1 nav-3-3"><a href="{{store direct_url='cats/foods.html'}}">Nulla Quam </a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/apparel.html'}}">Morbi Odio</a></li>
</ul>
</div>
<div class="col_1"><span class="title_col">Malesuada </span> 
<ul>
<li class="level1 nav-3-1 first"><a href="{{store direct_url='cats/foods.html'}}">Phasellus Purus</a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/foods.html'}}">Laoreet Sed</a></li>
<li class="level1 nav-3-3"><a href="{{store direct_url='cats/foods.html'}}">Nulla Quam </a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/apparel.html'}}">Morbi Odio</a></li>
</ul>
</div>
</div>
<div class="col_6">
<div class="col_2 firstcolumn">
<p class="note">Here is some contents with side images</p>
<div class="img"><a href="#"><img src="{{skin url='images/media/menu/image-product1.jpg'}}" alt="" width="100" height="100" /></a></div>
</div>
<div class="col_2">
<p class="first">Aliquam tristique faucibus metus malesuada liberom viverra at. Quisque ornare neque est.</p>
<p>Nam vehicula, dui in ultricies porttitorue non duieget aenean laoreet sapien id urna placerat  sollicitudins erat volutpat. Curabitur pretium, nisi vitae pretiumeo volutpat, ligula elit suscipit libero, et fringilla enimnu  Etiam sit amet sem nibh, id tincidunt diam.Feugiat ullamcorper venenatis vel, tincidunt sit amet justo.</p>
</div>
<div class="col_2">
<p class="note">This is a blackbox, you can use it to highlight some contens</p>
<p>Curabitur tempus tellus sit amet tristique comlectus nisi commodo libero, id cursus lacus nibh vitae nibh.</p>
<p>Nam vehicula, dui in ultricies porttitorue non duieget aenean laoreet sapien id urna placerat  sollicitudins erat volutpat. Curabitur pretium, nisi vitae pretiumeo volutpat, ligula elit suscipit libero, et fringilla enimnu  Etiam sit amet sem nibh, id tincidunt diam.Feugiat ullamcorper venenatis vel, tincidunt sit amet justo.</p>
</div>
</div>
</div>
</div>
<!-- End 6 column Item --></li>
<li class="submenu last"><a class="drop level-top" href="{{store direct_url='cats/foods.html'}}"><span>NEW PRODUCT </span></a>
<div class="dropdown_6columns">
<div class="inner"><span class="title">Dress Shirt</span>
<div class="col_2">
<div class="col_2 firstcolumn"><a href="#"><img src="{{skin url='images/media/menu/image-product1.jpg'}}" alt="" width="100" height="100" /></a>
<p>Quisque rhoncus mi non ligula iaculis eu consequat lectus eleifend. 	Vestibulum consectetur erat in lacus mollis luctus</p>
</div>
<div class="col_2"><span class="title_col">Last added</span>
<div class="product">{{widget type="catalog/product_widget_new" 	products_count="1" column_count="1" show_title="false" 	show_addtolinks="false" 	template="catalog/product/widget/new/content/new_grid.phtml"}}</div>
</div>
</div>
<div class="col_1"><span class="title_col">Pellentesque </span> 
<ul>
<li class="level1 nav-3-1 first"><a href="{{store direct_url='cats/foods.html'}}">Phasellus Purus</a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/foods.html'}}">Laoreet Sed</a></li>
<li class="level1 nav-3-3"><a href="{{store direct_url='cats/foods.html'}}">Nulla Quam </a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/apparel.html'}}">Morbi Odio</a></li>
<li class="level1 nav-3-5"><a href="{{store direct_url='cats/apparel.html'}}">Amet Bibendum </a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/apparel.html'}}">Tristique 		Turpis</a></li>
<li class="level1 nav-3-3"><a href="{{store direct_url='cats/foods.html'}}">Phasellus leo</a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/foods.html'}}">Amet Bibendum</a></li>
<li class="level1 nav-3-5"><a href="{{store direct_url='cats/foods.html'}}">Tristique Turpis</a></li>
</ul>
</div>
<div class="col_1"><span class="title_col">Pellentesque</span> 
<ul>
<li class="level1 nav-3-1 first"><a href="{{store direct_url='cats/foods.html'}}">Phasellus Purus</a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/foods.html'}}">Laoreet Sed</a></li>
<li class="level1 nav-3-3"><a href="{{store direct_url='cats/foods.html'}}">Nulla Quam </a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/apparel.html'}}">Morbi Odio</a></li>
<li class="level1 nav-3-5"><a href="{{store direct_url='cats/apparel.html'}}">Amet Bibendum </a></li>
<li class="level1 nav-3-2"><a href="{{store direct_url='cats/apparel.html'}}">Tristique 		Turpis</a></li>
<li class="level1 nav-3-4"><a href="{{store direct_url='cats/foods.html'}}">Amet Bibendum</a></li>
<li class="level1 nav-3-5"><a href="{{store direct_url='cats/foods.html'}}">Tristique Turpis</a></li>
</ul>
</div>
<div class="col_2 last"><span class="title_col">Malesuada Tristique</span>
<div class="product">{{widget type="mediauploadurlwidget/upload" 	media="http://www.youtube.com/watch?v=9py78_xgIKo&amp;feature=related" 	width="280" height="280" wmode="transparent"}}</div>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing Aliquam erat 	volutpat. Phasellus leo sapien</p>
</div>
</div>
</div>
</li>
</ul>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Special Product',
	'identifier' => $prefixBlock.'special_product',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
			<div>{{widget type="flexiblewidget/list" column_count="1" limit_count="1" attribute_set="Default" if="special_price >0" order_by="name asc" category="2" template="custom_template" custom_theme="flexiblewidget/special_product.phtml"}}</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Product Categories right',
	'identifier' => $prefixBlock.'product_cat_right',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
			<div class="block category-products ">
<div class="block-title"><strong><span>Reptiles</span></strong></div>
{{widget type="flexiblewidget/list" column_count="1" limit_count="1" attribute_set="Default" order_by="name asc" category="60" template="custom_template" custom_theme="flexiblewidget/product_cat_right.phtml"}}</div>
<div class="block category-products ">
<div class="block-title"><strong><span>Birds</span></strong></div>
{{widget type="flexiblewidget/list" column_count="1" limit_count="1" attribute_set="Default" order_by="name asc" category="54" template="custom_template" custom_theme="flexiblewidget/product_cat_right.phtml"}}</div>
EOB
);
$block->setData($dataBlock)->save();


$dataBlock = array(
	'title' => 'Verisign Callout Login',
	'identifier' => $prefixBlock.'verisign_callout',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
			<p><a title="verisign" href="#"><img src="{{skin url='images/media/verisign_callout.png'}}" alt="verisign" /></a></p>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Geotrust Callout Login',
	'identifier' => $prefixBlock.'geotrust_callout',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
			<p><a title="geotrust" href="#"><img src="{{skin url='images/media/geotrust_callout.png'}}" alt="geotrust" /></a></p>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Slideshow Bestseller',
	'identifier' => $prefixBlock.'slideshow_bestseller',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
			<div class="slideshow-bestseller">
<div class="slideshow-wrapper">{{widget type="bestsellerproducts/list" order_by="name asc" new_category="2" limit_count="8" column_count="8" choose_template="custom_template" custom_theme="em_bestseller_products/bestseller_product.phtml" cache_lifetime="3600"}}</div>
</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Lastest Review',
	'identifier' => $prefixBlock.'lastest_review',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
			<div class="lastest_reviews">{{block type="latestreviews/list" name="latestreviews.products" alias="ratings_product" template="morningtime/latestreviews/list-product.phtml"}}</div>
EOB
);
$block->setData($dataBlock)->save();


####################################################################################################
# INSERT PAGE
####################################################################################################

/*$dataPage = array(
	'title'				=> 'aaaa',
	'identifier' 		=> $prefixPage.'aaaa',
	'stores'			=> $stores,
	'content_heading'	=> '',
	'content'			=> <<<EOB
		&nbsp;
EOB
,
	'root_template'		=> 'two_columns_left',
);
$page->setData($dataPage)->save();*/

$dataPage = array(
	'title'				=> 'Home page GalaPeta',
	'identifier' 		=> $prefixPage.'home',
	'stores'			=> $stores,
	'content_heading'	=> '',
	'content'			=> <<<EOB
		<div class="feature-product">{{widget type="dynamicproducts/dynamicproducts" order_by="name asc" new_category="2" featured_choose="Featured" column_count="3" limit_count="9" choose_template="custom_template" custom_theme="em_featured_products/feature_product.phtml" cache_lifetime="3600"}}</div>
EOB
,
	'root_template'		=> 'two_columns_right',
);
$page->setData($dataPage)->save();


$installer->endSetup(); 
