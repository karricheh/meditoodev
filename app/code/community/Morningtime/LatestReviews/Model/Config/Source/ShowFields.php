<?php
/**
 * Morningtime LatestReviews extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Morningtime
 * @package    Morningtime_LatestReviews
 * @copyright  Copyright (c) 2009 Morningtime Internet, http://www.morningtime.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Morningtime_LatestReviews_Model_Config_Source_ShowFields
{
    public function toOptionArray()
    {
        return array(
            array('value'=>'date', 'label'=>Mage::helper('adminhtml')->__('Date')),
            array('value'=>'title', 'label'=>Mage::helper('adminhtml')->__('Title')),
            array('value'=>'preview', 'label'=>Mage::helper('adminhtml')->__('Preview')),
            array('value'=>'rating', 'label'=>Mage::helper('adminhtml')->__('Rating')),
        );
    }
}
