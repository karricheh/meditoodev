<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Paybox
 * @package     Paybox_Payment
 * @copyright   Copyright (c) 2011 Netapsys Conseil (http://www.netapsys.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Paybox System module
 *
 * @category    Paybox
 * @package     Paybox_Payment
 * @author      Netapsys Magento Team <magento@netapsys.fr>
 */
class Paybox_Payment_Block_System_Redirect extends Mage_Core_Block_Abstract
{
    
    /**
     * HTML rendering
     *
     * @return string
     * @codeCoverageIgnore
     */
    protected function _toHtml()
    {
        $system = $this->getOrder()->getPayment()->getMethodInstance();

        $form = new Varien_Data_Form();
        $form->setAction(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $system->getPayboxFile())
                ->setId('paybox_system_redirect_form')
                ->setName('paybox_system_redirect_form')
                ->setMethod('POST')
                ->setUseContainer(true);

        foreach ($system->getFormFields('form') as $field => $value) {
            $form->addField($field, 'hidden', array('name' => $field, 'value' => $value));
        }

        $html = '<div style="display:none;">';
        $html.= $form->toHtml();
        $html.= '<script type="text/javascript">document.getElementById("paybox_system_redirect_form").submit();</script>';
        $html.= '</div>';

        return $html;
    }
}
