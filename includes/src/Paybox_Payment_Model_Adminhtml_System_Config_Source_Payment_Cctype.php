<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Paybox
 * @package     Paybox_Payment
 * @copyright   Copyright (c) 2011 Netapsys Conseil (http://www.netapsys.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Paybox Credit Cards Source model
 *
 * @category    Paybox
 * @package     Paybox_Payment
 * @author      Netapsys Magento Team <magento@netapsys.fr>
 */
class Paybox_Payment_Model_Adminhtml_System_Config_Source_Payment_Cctype
{

    /**
     * Retreieve all available Paybox credit cards
     * 
     * @return array 
     */
    public function toOptionArray()
    {
        $options = array();
        $ccGroups = Mage::getConfig()->getNode('global/payment/paybox_cc/groups')->asArray();
        foreach ($ccGroups as $code => $group) {
            $options[(int) $group['order']] = array(
                'code' => $code,
                'label' => Mage::helper('paybox')->__($group['label']),
                'value' => array()
            );
        }
        //ksort($options);

        $ccTypes = Mage::getConfig()->getNode('global/payment/paybox_cc/pbx_typepaiement')->asArray();
        foreach ($ccTypes as $pbx_typepaiement) {
            foreach ($pbx_typepaiement['pbx_typecarte'] as $pbx_tyecarte) {
                $ccInfo = array(
                    'label' => Mage::helper('paybox')->__($pbx_tyecarte['name']),
                    'value' => $pbx_tyecarte['code']
                );

                $options[$this->_getGroupOrderByCode($pbx_tyecarte['group'])]['value'][(int) $pbx_tyecarte['order']] = $ccInfo;
            }
        }

        $optionArray = array();
        foreach ($options as $order => $group) {
            if (!empty($group['value'])) {
                ksort($group['value']);
                $optionArray[$order] = array(
                    'label' => $group['label'],
                    'value' => $group['value']
                );
            }
        }
        ksort($optionArray);

        return $optionArray;
    }

    /**
     * Retrieve group order from his code (for ordered groups)
     * 
     * @param string $groupCode
     * @return int 
     */
    protected function _getGroupOrderByCode($groupCode)
    {
        $group = Mage::getConfig()->getNode('global/payment/paybox_cc/groups/' . $groupCode)->asArray();

        return (int) $group['order'];
    }

}
