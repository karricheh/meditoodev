<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Paybox
 * @package     Paybox_Payment
 * @copyright   Copyright (c) 2011 Netapsys Conseil (http://www.netapsys.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Config model that is aware of all Paybox_Payment payment methods
 * 
 * @category    Paybox
 * @package     Paybox_Payment
 * @author      Netapsys Magento Team <magento@netapsys.fr>
 */
class Paybox_Payment_Model_Config
{
    /**
     * Retrieve array of credit card types
     *
     * @return  array
     */
    public function getCcTypes()
    {
        $_types = Mage::getConfig()->getNode('global/payment/paybox_cc/pbx_typepaiement')->asArray();

        $types = array();
        foreach ($_types as $type) {
            foreach ($type['pbx_typecarte'] as $data) {
                if (isset($data['code']) && isset($data['name'])) {
                    $types[$data['code']] = $data['name'];
                }
            }
        }
        return $types;
    }
    
    /**
     * Retrieve payment type by credit card type
     * 
     * @param string $ccType
     * @return string|bool
     */
    public function getPaiementTypeByCcType($ccType)
    {
        $types = Mage::getConfig()->getNode('global/payment/paybox_cc/pbx_typepaiement')->asArray();

        foreach ($types as $type) {
            foreach ($type['pbx_typecarte'] as $data) {
                if ($data['code'] == $ccType) {
                    return $type['code'];
                }
            }
        }
        return false;
    }
}

