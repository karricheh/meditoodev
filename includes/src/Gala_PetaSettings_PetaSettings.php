<?php
class Gala_PetaSettings_PetaSettings
{
	public function get_grid_thumb_width()
	{
		return Mage::getStoreConfig('peta/image/grid_thumb_width');
	}
	public function get_grid_thumb_height()
	{
		return Mage::getStoreConfig('peta/image/grid_thumb_height');
	}
	public function get_grid_thumb_bgcolor()
	{
		return Mage::getStoreConfig('peta/image/grid_thumb_bgcolor');
	}
	public function get_listing_thumb_width()
	{
		return Mage::getStoreConfig('peta/image/listing_thumb_width');
	}
	public function get_listing_thumb_height()
	{
		return Mage::getStoreConfig('peta/image/listing_thumb_height');
	}
	public function get_listing_thumb_bgcolor()
	{
		return Mage::getStoreConfig('peta/image/listing_thumb_bgcolor');
	}
	public function get_base_image_width()
	{
		return Mage::getStoreConfig('peta/image/base_image_width');
	}
	public function get_base_image_height()
	{
		return Mage::getStoreConfig('peta/image/base_image_height');
	}
	public function get_base_image_bgcolor()
	{
		return Mage::getStoreConfig('peta/image/base_image_bgcolor');
	}
	public function get_thumb_base_width()
	{
		return Mage::getStoreConfig('peta/image/thumb_base_width');
	}
	public function get_thumb_base_height()
	{
		return Mage::getStoreConfig('peta/image/thumb_base_height');
	}
	public function get_thumb_base_bgcolor()
	{
		return Mage::getStoreConfig('peta/image/thumb_base_bgcolor');
	}
	public function get_related_width()
	{
		return Mage::getStoreConfig('peta/image/related_width');
	}
	public function get_related_height()
	{
		return Mage::getStoreConfig('peta/image/related_height');
	}
	public function get_related_bgcolor()
	{
		return Mage::getStoreConfig('peta/image/related_bgcolor');
	}
	public function get_crosssell_width()
	{
		return Mage::getStoreConfig('peta/image/crosssell_width');
	}
	public function get_crosssell_height()
	{
		return Mage::getStoreConfig('peta/image/crosssell_height');
	}
	public function get_crosssell_bgcolor()
	{
		return Mage::getStoreConfig('peta/image/crosssell_bgcolor');
	}
	public function get_upsell_width()
	{
		return Mage::getStoreConfig('peta/image/upsell_width');
	}
	public function get_upsell_height()
	{
		return Mage::getStoreConfig('peta/image/upsell_height');
	}
	public function get_upsell_bgcolor()
	{
		return Mage::getStoreConfig('peta/image/upsell_bgcolor');
	}
	public function get_widget_width()
	{
		return Mage::getStoreConfig('peta/image/widget_width');
	}
	public function get_widget_height()
	{
		return Mage::getStoreConfig('peta/image/widget_height');
	}
	public function get_widget_bgcolor()
	{
		return Mage::getStoreConfig('peta/image/widget_bgcolor');
	}
}