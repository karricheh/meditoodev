<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Paybox
 * @package     Paybox_Payment
 * @copyright   Copyright (c) 2011 Netapsys Conseil (http://www.netapsys.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Paybox System module
 *
 * @category    Paybox
 * @package     Paybox_Payment
 * @author      Netapsys Magento Team <magento@netapsys.fr>
 */
class Paybox_Payment_Model_System extends Mage_Payment_Model_Method_Abstract
{

    protected $_code = 'paybox_system';
    protected $_accountPath = 'paybox/account/';
    protected $_systemGlobalPath = 'paybox/payboxsystem_global/';
    protected $_mode = array('form' => 1, 'commandline' => 4);
    protected $_isGateway = false;
    protected $_canAuthorize = true;
    protected $_canCapture = false;
    protected $_canCapturePartial = false;
    protected $_canRefund = false;
    protected $_canVoid = true;
    protected $_canUseInternal = false;
    protected $_canUseCheckout = true;
    protected $_canUseForMultishipping = false;
    protected $_canFetchTransactionInfo = true;
    protected $_formBlockType = 'paybox/system_form';
    protected $_order;

    /**
     * Returl redirect URL to checkout
     *
     * @return string
     */
    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('paybox/system/redirect', array('_secure' => true));
    }

    /**
     * Get order model
     *
     * @return Mage_Sales_Model_Order
     * @codeCoverageIgnore
     */
    public function getOrder()
    {
        if (!$this->_order) {
            // @codeCoverageIgnoreStart
            $paymentInfo = $this->getInfoInstance();
            $this->_order = Mage::getModel('sales/order')
                    ->loadByIncrementId($paymentInfo->getOrder()->getRealOrderId());
            // @codeCoverageIgnoreEnd
        }
        return $this->_order;
    }

    /**
     * Set order
     *
     * @param Mage_Sales_Model_Order $order
     * @return Paybox_Payment_Model_System
     */
    public function setOrder(Mage_Sales_Model_Order $order)
    {
        $this->_order = $order;
        return $this;
    }

    /**
     * Get payment action from PBX_AUTOSEULE data
     *
     * @return string
     */
    public function getConfigPaymentAction()
    {
        return ($this->getAuthorizeOnly() == 'O') ? Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE : Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE;
    }

    /**
     * Get success path for URL
     *
     * @return string
     */
    public function getPathEffectue()
    {
        return $this->getConfigData('pbx_effectue');
    }

    /**
     * Get payment refused path for URL
     *
     * @return string
     */
    public function getPathRefuse()
    {
        return $this->getConfigData('pbx_refuse');
    }

    /**
     * Get payment canceled path for URL
     *
     * @return string
     */
    public function getPathAnnule()
    {
        return $this->getConfigData('pbx_annule');
    }

    /**
     * Get automatic response path for URL
     *
     * @return string
     */
    public function getPathRepondreA()
    {
        return $this->getConfigData('pbx_repondre_a');
    }

    /**
     * Get error path for URL
     *
     * @return string
     */
    public function getPathErreur()
    {
        return $this->getConfigData('pbx_erreur');
    }

    /**
     * Get name of executable file
     *
     * @return string
     */
    public function getPayboxFile()
    {
        //return $this->getConfigData('cgi_file_path');
        return Mage::getStoreConfig($this->_systemGlobalPath . 'cgi_file_path', $this->getStore());
    }

    /**
     * Get AuthorizationOnly flag
     *
     * @return string
     */
    public function getAuthorizeOnly()
    {
        return $this->getConfigData('authorize_only');
    }

    /**
     * Get CC choice
     *
     * @return int
     */
    public function getCcChoice()
    {
        return $this->getConfigData('cc_choice');
    }

    /**
     * Get Ruf1 (GET or POST)
     *
     * @return string
     */
    public function getRuf1()
    {
        return $this->getConfigData('pbx_ruf1');
    }

    /**
     * Get test mode status
     *
     * @return string
     */
    public function getTestModeStatus()
    {
        return $this->getConfigData('test_mode');
    }

    /**
     * Get Paybox URL for test mode
     *
     * @return string
     */
    public function getTestModeUrl()
    {
        return $this->getConfigData('test_mode_url');
    }

    /**
     * Get Payment validated status
     *
     * @todo
     * @return string
     */
//    public function getPaymentValidatedStatus()
//    {
//        return $this->getConfigData('payment_validated_status');
//    }

    /**
     * Get Payment refused status
     *
     * @todo
     * @return string
     */
//    public function getPaymentRefusedStatus()
//    {
//        return $this->getConfigData('payment_refused_status');
//    }

    /**
     * Get Site number (TPE)
     *
     * @return string
     */
    public function getSiteNumber()
    {
        return Mage::getStoreConfig($this->_accountPath . 'pbx_site', $this->getStore());
    }

    /**
     * Get Rang number
     *
     * @return string
     */
    public function getRang()
    {
        return Mage::getStoreConfig($this->_accountPath . 'pbx_rang', $this->getStore());
    }

    /**
     * Get Identifiant number
     *
     * @return string
     */
    public function getIdentifiant()
    {
        return Mage::getStoreConfig($this->_accountPath . 'pbx_identifiant', $this->getStore());
    }

    /**
     * Get currency number in ISO4217 format
     *
     * @return string
     */
    public function getCurrencyNumber()
    {
        $currencyCode = $this->getOrder()->getBaseCurrencyCode();
        $currenciesNumbers = Mage::getStoreConfig('paybox/currencies');

        if ($currencyNum = $currenciesNumbers[$currencyCode]) {
            return (string) $currencyNum;
        } else {
            return (string) $currenciesNumbers[Mage::getStoreConfig('currency/options/default')];
        }
    }

    /**
     * Get language of interface of payment defined in config
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->getConfigData('pbx_langue');
    }

    /**
     * Command line mode
     *
     * @return string
     * @codeCoverageIgnore
     */
    public function processCommandLineMode()
    {
        $fieldsArr = $this->getFormFields('commandline');
        $paramStr = '';
        foreach ($fieldsArr as $k => $v) {
            $paramStr .= $k . '=' . $v . ' ';
        }
        $paramStr = str_replace(';', '\;', $paramStr);

        // Forbidden shell_exec ?
        if (strstr(ini_get('disable_functions'), 'shell_exec')) {
            Mage::throwException('Forbidden `shell_exec` command.');
        }

        if (!$result = shell_exec(Mage::getBaseDir() . '/' . $this->getPayboxFile() . ' ' . $paramStr)) {
            Mage::throwException('No result from binary file.');
        }

        if (strstr($result, 'PAYBOX INPUT ERROR')) {
            Mage::throwException('Error while executing binary file : %s', $result);
        }

        return $result;
    }

    /**
     * Building array of params to send
     *
     * @param string $_paymentMethod
     * @return array
     * @codeCoverageIgnore
     */
    public function getFormFields($_paymentMethod = 'commandline')
    {
        $fieldsArr = array();

        $fieldsArr = array(
            'PBX_MODE' => $this->_mode[$_paymentMethod],
            'PBX_SITE' => $this->getSiteNumber(),
            'PBX_RANG' => $this->getRang(),
            'PBX_IDENTIFIANT' => $this->getIdentifiant(),
            'PBX_TOTAL' => (int) ($this->getOrder()->getBaseGrandTotal() * 100),
            'PBX_DEVISE' => $this->getCurrencyNumber(),
            'PBX_CMD' => $this->getOrder()->getRealOrderId(),
            'PBX_PORTEUR' => $this->getOrder()->getCustomerEmail(),
            'PBX_RETOUR' => 'amount:M;ref:R;auto:A;trans:T;error:E;paiement:P;carte:C;idtrans:S;validite:D;sign:K',
            'PBX_EFFECTUE' => Mage::getUrl($this->getPathEffectue(), array('_secure' => true)),
            'PBX_REFUSE' => Mage::getUrl($this->getPathRefuse(), array('_secure' => true)),
            'PBX_ANNULE' => Mage::getUrl($this->getPathAnnule(), array('_secure' => true)),
            'PBX_REPONDRE_A' => Mage::getUrl($this->getPathRepondreA(), array('_secure' => true)),
            'PBX_AUTOSEULE' => $this->getAuthorizeOnly(),
            'PBX_LANGUE' => $this->getLanguage(),
            'PBX_ERREUR' => Mage::getUrl($this->getPathErreur(), array('_secure' => true)),
            'PBX_RUF1' => $this->getRuf1(),
        );
        
        if ($this->getCcChoice() == 1) {
            $ccType = $this->getOrder()->getPayment()->getData('cc_type');
            $paymentType = Mage::getModel('paybox/config')->getPaiementTypeByCcType($ccType);
            $fieldsArr['PBX_TYPEPAIEMENT'] = $paymentType;
            $fieldsArr['PBX_TYPECARTE'] = $ccType;
        }

        if ($_paymentMethod == 'commandline' && $this->getPingFlag()) {
            $tmpFieldsArr['PBX_PING'] = '1';
            if (($pingPort = trim($this->getPingPort())) != '') {
                $tmpFieldsArr['PING_PORT'] = $pingPort;
            }

            $fieldsArr = array_merge($fieldsArr, $tmpFieldsArr);
        }

        // 'E' for automatic redirect
        $fieldsArr['PBX_OUTPUT'] = 'E';

        $this->_manageIntermediatePage($fieldsArr);

        if ($this->getTestModeStatus()) {
            $fieldsArr['PBX_PAYBOX'] = $this->getTestModeUrl();
        }

        return $fieldsArr;
    }

    /**
     * Set data for inermediate page
     * 
     * @param array &$fieldsArr
     * @return void
     * @codeCoverageIgnore
     */
    private function _manageIntermediatePage(&$fieldsArr)
    {
        $fieldsArr['PBX_TXT'] = Mage::helper('paybox')->__('Processing redirection ...');
        $fieldsArr['PBX_WAIT'] = 0;
        $fieldsArr['PBX_BOUTPI'] = null;
    }

    /**
     * Capture
     *
     * @deprecated
     * @return Paybox_Payment_Model_System
     */
//    public function capture(Varien_Object $payment, $amount)
//    {
//        $payment->setStatus(self::STATUS_APPROVED)
//                ->setLastTransId($this->getTransactionId());
//        return $this;
//    }

    /**
     * Validate payment method information object
     *
     * @deprecated
     */
//    public function validate()
//    {
//        $quote = Mage::getSingleton('checkout/session')->getQuote();
//        $quote->setCustomerNoteNotify(false);
//        parent::validate();
//    }

    /**
     * Authorize
     *
     * @deprecated
     * @return Paybox_Payment_Model_System
     */
//    public function authorize(Varien_Object $payment, $amount)
//    {
//        $payment->setStatus(self::STATUS_APPROVED)
//                ->setLastTransId($this->getTransactionId());
//
//        return $this;
//    }

    /**
     * Cancel
     *
     * @deprecated
     * @return Paybox_Payment_Model_System
     */
//    public function cancel(Varien_Object $payment)
//    {
//        $payment->setStatus(self::STATUS_DECLINED);
//        return $this;
//    }

    /**
     * IPN for Instant Payment Notification
     * when Paybox server call Magento to validate (no not) payment
     *
     * @param array $params
     * @return void
     * @codeCoverageIgnore
     */
    public function processIpnRequest($params)
    {
        try {
            $order = Mage::getModel('sales/order')
                    ->loadByIncrementId($params['ref']);

            $this->_checkPayboxParams($order, $params, 'ipn');

            if ($params['error'] == '00000') {
                // Payment accepted by Paybox
                // State and status Processing
                $order->setState(
                        Mage_Sales_Model_Order::STATE_PROCESSING, Mage_Sales_Model_Order::STATE_PROCESSING, Mage::helper('paybox')->__('Payment accepted by Paybox')
                );

                // Save transaction                
                $payment = $order->getPayment();
                $payment->setTransactionId($params['trans']);
                $transaction = $payment->addTransaction(
                        Mage_Sales_Model_Order_Payment_Transaction::TYPE_ORDER, null, false, 'Paybox : '
                );
                $transaction->setAdditionalInformation(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS, $params);
                $transaction->setIsClosed(true);

                $payment->setData('cc_trans_id', $params['trans']);
                
                if (isset($params['nb_paie']))
                    $payment->setData('paybox_nb_paie', $params['nb_paie']);

                /**
                 * @deprecated 
                 */
                // Billing creation
//                if ($this->_createInvoice($order, $params)) {
//                    $order->addStatusToHistory($order->getStatus(), Mage::helper('paybox')->__('Invoice successfully created'));
//                } else {
//                    $order->addStatusToHistory($order->getStatus(), Mage::helper('paybox')->__('Can\'t create invoice'));
//                }
                // Email notification
                if (!$order->getEmailSent()) {
                    $order->sendNewOrderEmail();
                }
            } else {
                // Payment refused by Paybox
                $errorMsg = Mage::helper('paybox')->__('Payment refused by Paybox');
                $errorMsg .= ' - ';
                $errorMsg .= Mage::helper('paybox')->__('Error code: %s', $params['error']);

                // Order cancel
                $order->addStatusToHistory(
                        Mage_Sales_Model_Order::STATE_HOLDED, $errorMsg
                );
                $order->hold();
            }

            $order->save();
        } catch (Exception $e) {
            $order->addStatusToHistory(
                    $order->getStatus(), Mage::helper('paybox')->__('Error in order validation : %s', $e->getMessage())
            )->save();
        }
    }

    /**
     * When customer return on Magento after payment accepted
     *
     * @param array $params
     * @return void
     * @codeCoverageIgnore
     */
    public function processSuccessRequest($params)
    {
        $this->_checkSession();
        $order = Mage::getModel('sales/order')
                ->loadByIncrementId($params['ref']);

        $this->_checkPayboxParams($order, $params);
        $session = Mage::getSingleton('checkout/session');
        $session->unsPayboxOrderId();

        $order->addStatusToHistory(
                $order->getStatus(), Mage::helper('paybox')->__('Customer returned from Paybox')
        )->save();

        $session->setQuoteId($session->getPayboxQuoteId(true));
        $session->getQuote()->setIsActive(false)->save();
        $session->unsPayboxQuoteId();
        $session->setCanRedirect(false);
    }

    /**
     * When customer return on Magento after payment refused
     *
     * @param array $params
     * @return string
     * @codeCoverageIgnore
     */
    public function processRefuseRequest($params)
    {
        $this->_checkSession();
        $order = Mage::getModel('sales/order')
                ->loadByIncrementId($params['ref']);

        $this->_checkPayboxParams($order, $params);
        $session = Mage::getSingleton('checkout/session');
        $session->unsPayboxOrderId();
        $helper = Mage::helper('paybox');
        $session->setPayboxErrorMessage($helper->__('Payment was rejected by Paybox'));

        $errMsg = $helper->__('Payment refused by Paybox');
        if (array_key_exists('error', $params)) {
            $errMsg .= ' - ' . $helper->__('Error code: %s', $params['error']);
        }

        return $errMsg;
    }

    /**
     * When customer return on Magento without paying
     *
     * @param array $params
     * @return void
     * @codeCoverageIgnore
     */
    public function processDeclineRequest($params)
    {
        $this->_checkSession();
        $order = Mage::getModel('sales/order')
                ->loadByIncrementId($params['ref']);

        $this->_checkPayboxParams($order, $params, 'decline');
        $session = Mage::getSingleton('checkout/session');

        // Order cancel
        $order->addStatusToHistory(
                Mage_Sales_Model_Order::STATE_CANCELED, Mage::helper('paybox')->__('Order was canceled by customer')
        );

        $order->cancel();
        $order->save();
        $session->addNotice(Mage::helper('paybox')->__('Your payment was canceled.'));
        $session->setQuoteId($session->getPayboxQuoteId(true));
        $session->getQuote()
                ->setIsActive(false)
                ->save();
        $session->unsPayboxQuoteId();
    }

    /**
     * Creating invoice
     *
     * @deprecated
     * @param Mage_Sales_Model_Order $order
     * @return Mage_Sales_Model_Order_Invoice
     */
//    protected function _createInvoice(Mage_Sales_Model_Order $order, $params)
//    {
//        $invoice = $order->prepareInvoice()
//                ->setTransactionId($params['trans'])
//                ->addComment(Mage::helper('paybox')->__('Auto-generated from Paybox'))
//                ->register()
//                ->pay();
//
//        $transactionSave = Mage::getModel('core/resource_transaction')
//                ->addObject($invoice)
//                ->addObject($invoice->getOrder());
//
//        $transactionSave->save();
//
//        return $invoice;
//    }

    /**
     * Check params from Paybox
     *
     * @param Mage_Sales_Model_Order $order
     * @param array $params
     * @param string $action
     * @return bool
     * @codeCoverageIgnore
     */
    protected function _checkPayboxParams($order, $params, $action = null)
    {
        $helper = Mage::helper('paybox');

        // Check order reference ID
        if (!$order->getId()) {
            Mage::throwException($helper->__('Bad order reference : %s', $params['ref']));
        }

        if ($action !== 'ipn') {
            // Check order ID
            if (Mage::helper('core')->decrypt(Mage::getSingleton('checkout/session')->getPayboxOrderId()) != $params['ref']) {
                Mage::throwException($helper->__('Bad order reference : %s', $params['ref']));
            }
        }

        if ($action !== 'decline') {
            if (!isset($params['error']) || !isset($params['amount']) || !isset($params['ref']) || !isset($params['trans'])) {
                Mage::throwException($helper->__("Missing data"));
            }

            // Check amount
            if ((int) ($order->getBaseGrandTotal() * 100) != (int) $params['amount']) {
                Mage::throwException($helper->__("Amount does'nt match"));
            }
        }

        return true;
    }

    /**
     * Checking response and Paybox session variables
     *
     * @return bool
     * @codeCoverageIgnore
     */
    protected function _checkSession()
    {
        $session = Mage::getSingleton('checkout/session');
        if (!$session->getPayboxQuoteId() || !$session->getPayboxOrderId() || !$session->getPayboxPaymentAction()) {
            Mage::throwException(Mage::helper('paybox')->__('Session error'));
        }

        return true;
    }

}
