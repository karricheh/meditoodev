<?php

class Kiala_LocateAndSelect_Model_Language extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('locateandselect/language');
    }

}