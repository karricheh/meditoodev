<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Paybox
 * @package     Paybox_Payment
 * @copyright   Copyright (c) 2011 Netapsys Conseil (http://www.netapsys.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Paybox System module
 *
 * @category    Paybox
 * @package     Paybox_Payment
 * @author      Netapsys Magento Team <magento@netapsys.fr>
 */
class Paybox_Payment_SystemController extends Mage_Core_Controller_Front_Action
{

    /**
     * Get System Model
     *
     * @return  Paybox_Payment_Model_System
     */
    public function getPaymentModel()
    {
        return Mage::getSingleton('paybox/system');
    }
    
    /**
     * Redirect customer from Magento to Paybox
     *
     * @return void
     */
    public function redirectAction()
    {
        $session = Mage::getSingleton('checkout/session');
        $session->setPayboxQuoteId($session->getQuoteId());

        $order = Mage::getModel('sales/order')
                ->loadByIncrementId($session->getLastRealOrderId());
        
        $session->setPayboxOrderId(Mage::helper('core')->encrypt($session->getLastRealOrderId()));
        $session->setPayboxPaymentAction(
                $order->getPayment()->getMethodInstance()->getAuthorizeOnly()
        );

        $session->unsQuoteId();

        $this->getPaymentModel()->setOrder($order);
        
        try {
            // First try commandline mode
            $result = $this->getPaymentModel()->processCommandLineMode();
            $order->addStatusToHistory(
                $order->getStatus(), $this->__('Customer was redirected to Paybox (commandeline mode)')
            );
            $order->save();
            
            $this->loadLayout(false);
            $result = '<div style="display:none;">'.$result.'</div>';
            $this->getResponse()->setBody($result);
            $this->renderLayout();

        } catch (Exception $e) {
            Mage::log($e->getMessage());
            
            // Second form mode
            $order->addStatusToHistory(
                $order->getStatus(), $this->__('Customer was redirected to Paybox (form mode)')
            );
            $order->save();
            
            $this->getResponse()->setBody(
                $this->getLayout()
                    ->createBlock('paybox/system_redirect')
                    ->setOrder($order)
                    ->toHtml()
            );
        }

        $session->unsQuoteId();
    }

    /**
     * IPN for Instant Payment Notification
     * when Paybox server call Magento to validate (no not) payment 
     *
     * @return void
     */
    public function ipnAction()
    {
        try {
            $params = $this->getRequest()->getParams();

            // Launch IPN process
            $this->getPaymentModel()->processIpnRequest($params);
            
            // Blank page
            exit();
            
        } catch (Exception $e) {
            Mage::logException($e);
        }

        $this->loadLayout();
        $this->renderLayout();
    }
    
    /**
     * When customer return on Magento after payment accepted
     * 
     * @return void
     */
    public function successAction()
    {
        try {
            $params = $this->getRequest()->getParams();
            
            // Launch Success process
            $this->getPaymentModel()->processSuccessRequest($params);
            
            // Redirect to success page
            $this->_redirect('checkout/onepage/success');
            
        } catch (Exception $e) {
            Mage::logException($e);
            exit($e->getMessage());
        }
    }
    
    /**
     * When customer return on Magento after payment refused
     *
     * @return void
     */
    public function refuseAction()
    {
        try {
            $params = $this->getRequest()->getParams();
            
            // Launch Refuse process
            $this->getPaymentModel()->processRefuseRequest($params);
            
            // Redirect to failure page
            $this->_redirect('checkout/onepage/failure');
            
        } catch (Exception $e) {
            Mage::logException($e);
            exit($e->getMessage());
        }
    }
    
    /**
     * When customer return on Magento without paying
     *
     * @return void
     */
    public function declineAction()
    {
        try {
            $params = $this->getRequest()->getParams();
            
            // Launch Refuse process
            $this->getPaymentModel()->processDeclineRequest($params);
            
            // Redirect to failure page
            $this->_redirect('checkout/cart');
            
        } catch (Exception $e) {
            Mage::logException($e);
            exit($e->getMessage());
        }
    }
    
    /**
     * When Paybox encountered an error
     *
     * @return void
     */
    public function errorAction()
    {
        $numErr = $this->getRequest()->getParam('NUMERR');
        
        if (!Mage::getSingleton('checkout/session')->getPayboxQuoteId() || !$numErr) {
            exit(Mage::helper('paybox')->__('Session error'));
        }

        exit(Mage::helper('paybox')->__('Paybox error %s', $numErr));
    }
              
}
