<?php
/**
 * Morningtime LatestReviews extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Morningtime
 * @package    Morningtime_LatestReviews
 * @copyright  Copyright (c) 2009 Morningtime Internet, http://www.morningtime.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Morningtime_LatestReviews_Model_Config_Source_SortFields
{
    public function toOptionArray()
    {
        return array(
            array('value'=>'rs.rating_summary', 'label'=>Mage::helper('adminhtml')->__('Best rated first')),
            array('value'=>'r.created_at', 'label'=>Mage::helper('adminhtml')->__('Most recent first')),
        );
    }
}
