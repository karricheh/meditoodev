<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Paybox
 * @package     Paybox_Payment
 * @copyright   Copyright (c) 2011 Netapsys Conseil (http://www.netapsys.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Paybox System form
 *
 * @category    Paybox
 * @package     Paybox_Payment
 * @author      Netapsys Magento Team <magento@netapsys.fr>
 */
class Paybox_Payment_Block_System_Form extends Mage_Payment_Block_Form
{

    /**
     * Class constructor - Sets form template
     * 
     * @return void 
     * @codeCoverageIgnore
     */
    protected function _construct()
    {
        $this->setTemplate('paybox/system/form.phtml');
        parent::_construct();
    }

    /**
     * Retrieve Paybox configuration object
     *
     * @return Paybox_Payment_Model_Config
     */
    protected function _getConfig()
    {
        return Mage::getSingleton('paybox/config');
    }

    /**
     * Retrieve cc_choice config
     * 
     * @return bool
     */
    public function getCcChoice()
    {
        return (bool)Mage::getStoreConfig('payment/paybox_system/cc_choice', $this->getStore());
    }

    /**
     * Retrieve availables Paybox credit card types
     *
     * @return array
     */
    public function getCcAvailableTypes()
    {
        $types = $this->_getConfig()->getCcTypes();
        if ($method = $this->getMethod()) {
            $availableTypes = $method->getConfigData('cctypes');
            if ($availableTypes) {
                $availableTypes = explode(',', $availableTypes);
                foreach (array_keys($types) as $code) {
                    if (!in_array($code, $availableTypes)) {
                        unset($types[$code]);
                    }
                }
            }
        }
        return $types;
    }

}
