CREATE TABLE `udshipclass_vendor` (
  `class_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) NOT NULL,
  `country_id` varchar(2) NOT NULL,
  `region_id` int(11) NOT NULL,
  `postcode` text,
  `sort_order` smallint(6) NOT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8