CREATE TABLE `udropship_vendor_statement_adjustment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `adjustment_id` varchar(64) DEFAULT NULL,
  `statement_id` varchar(30) DEFAULT NULL,
  `po_id` varchar(50) NOT NULL DEFAULT '',
  `amount` decimal(12,4) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `paid` tinyint(4) DEFAULT NULL,
  `comment` text,
  `adjustment_prefix` varchar(64) DEFAULT NULL,
  `po_type` varchar(32) DEFAULT 'shipment',
  `username` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_ADJUSTMENT_ID` (`adjustment_id`),
  KEY `IDX_STATEMENT_ID` (`statement_id`),
  KEY `IDX_CREATED_AT` (`created_at`),
  KEY `IDX_PO_ID` (`po_id`,`po_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8