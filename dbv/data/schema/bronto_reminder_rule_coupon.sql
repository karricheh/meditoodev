CREATE TABLE `bronto_reminder_rule_coupon` (
  `rule_id` int(10) unsigned NOT NULL,
  `coupon_id` int(10) unsigned DEFAULT NULL,
  `unique_id` varchar(20) NOT NULL,
  `store_id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `visitor_id` int(10) unsigned NOT NULL DEFAULT '0',
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0',
  `wishlist_id` int(10) unsigned NOT NULL DEFAULT '0',
  `associated_at` datetime NOT NULL,
  `emails_failed` smallint(5) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`rule_id`,`unique_id`,`store_id`,`customer_id`,`visitor_id`,`quote_id`,`wishlist_id`),
  KEY `IDX_BRONTO_REMINDER_RULE_COUPON` (`rule_id`),
  CONSTRAINT `FK_BRONTO_REMINDER_RULE_COUPON` FOREIGN KEY (`rule_id`) REFERENCES `bronto_reminder_rule` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8