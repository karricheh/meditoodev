CREATE TABLE `udropship_batch_invrow` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `batch_id` int(10) unsigned NOT NULL,
  `product_id` text,
  `sku` varchar(50) DEFAULT NULL,
  `vendor_sku` varchar(50) DEFAULT NULL,
  `vendor_cost` decimal(12,4) DEFAULT NULL,
  `stock_qty` decimal(12,4) DEFAULT NULL,
  `has_error` tinyint(4) DEFAULT NULL,
  `error_info` text,
  `row_json` text,
  `stock_qty_add` decimal(12,4) DEFAULT NULL,
  `vendor_title` varchar(255) DEFAULT NULL,
  `vendor_price` decimal(12,4) DEFAULT NULL,
  `shipping_price` decimal(12,4) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `state` varchar(32) DEFAULT NULL,
  `avail_state` varchar(32) DEFAULT NULL,
  `avail_date` datetime DEFAULT NULL,
  `special_price` decimal(12,4) DEFAULT NULL,
  `special_from_date` datetime DEFAULT NULL,
  `special_to_date` datetime DEFAULT NULL,
  `stock_status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `FK_udropship_batch_invrow` (`batch_id`),
  CONSTRAINT `FK_udropship_batch_invrow` FOREIGN KEY (`batch_id`) REFERENCES `udropship_batch` (`batch_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8