CREATE TABLE `udropship_label_shipment` (
  `batch_id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `shipment_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `batch_id` (`batch_id`,`order_id`,`shipment_id`),
  KEY `order_id` (`order_id`),
  KEY `shipment_id` (`shipment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8