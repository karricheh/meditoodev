CREATE TABLE `udropship_shipping_website` (
  `shipping_id` int(10) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  UNIQUE KEY `website_id` (`website_id`,`shipping_id`),
  KEY `FK_udropship_shipping_website` (`shipping_id`),
  CONSTRAINT `FK_udropship_shipping_website` FOREIGN KEY (`shipping_id`) REFERENCES `udropship_shipping` (`shipping_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_udropship_shipping_website_pk` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8