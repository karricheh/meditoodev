CREATE TABLE `udropship_batch` (
  `batch_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` int(10) unsigned NOT NULL,
  `batch_type` varchar(50) NOT NULL,
  `batch_status` varchar(20) DEFAULT NULL,
  `rows_text` longtext,
  `num_rows` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `scheduled_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `notes` text,
  `error_info` text,
  `adapter_type` varchar(100) DEFAULT NULL,
  `use_wildcard` tinyint(4) DEFAULT NULL,
  `per_po_rows_text` longtext,
  `use_custom_template` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`batch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8