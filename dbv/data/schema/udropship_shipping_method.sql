CREATE TABLE `udropship_shipping_method` (
  `shipping_id` int(10) unsigned NOT NULL,
  `carrier_code` varchar(100) NOT NULL DEFAULT '',
  `method_code` varchar(255) NOT NULL DEFAULT '',
  KEY `FK_udropship_shipping_method` (`shipping_id`),
  CONSTRAINT `FK_udropship_shipping_method` FOREIGN KEY (`shipping_id`) REFERENCES `udropship_shipping` (`shipping_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8