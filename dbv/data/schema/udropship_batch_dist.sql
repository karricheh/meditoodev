CREATE TABLE `udropship_batch_dist` (
  `dist_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `batch_id` int(10) unsigned NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `dist_status` varchar(20) DEFAULT NULL,
  `error_info` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`dist_id`),
  KEY `FK_udropship_batch_dist` (`batch_id`),
  KEY `dist_status` (`dist_status`),
  CONSTRAINT `FK_udropship_batch_dist` FOREIGN KEY (`batch_id`) REFERENCES `udropship_batch` (`batch_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8