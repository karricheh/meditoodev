CREATE TABLE `usimplelic_license` (
  `license_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `license_key` varchar(255) NOT NULL DEFAULT '',
  `license_status` varchar(50) NOT NULL,
  `last_checked` datetime DEFAULT NULL,
  `last_status` varchar(20) DEFAULT NULL,
  `last_error` text,
  `retry_num` tinyint(4) DEFAULT NULL,
  `products` text NOT NULL,
  `server_restriction` text NOT NULL,
  `license_expire` datetime DEFAULT NULL,
  `upgrade_expire` datetime DEFAULT NULL,
  `signature` text,
  `server_info` text,
  `aux_checksum` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`license_id`),
  UNIQUE KEY `IDX_license_key` (`license_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8