CREATE TABLE `udropship_shipping_title` (
  `title_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shipping_id` int(10) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`title_id`),
  UNIQUE KEY `UNQ_UDSHIP_TITLE_SHIP_ID_STORE_ID` (`shipping_id`,`store_id`),
  KEY `IDX_UDSHIP_TITLE_STORE_ID` (`store_id`),
  KEY `IDX_UDSHIP_TITLE_SHIP_ID` (`shipping_id`),
  CONSTRAINT `FK_UDSHIP_TITLE_PARENT` FOREIGN KEY (`shipping_id`) REFERENCES `udropship_shipping` (`shipping_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_UDSHIP_TITLE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8