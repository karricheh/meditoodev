CREATE TABLE `label_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created Date',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated Date',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Productlabels Entity Table'