CREATE TABLE `bronto_reminder_rule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `conditions_serialized` mediumtext NOT NULL,
  `condition_sql` mediumtext,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `salesrule_id` int(10) unsigned DEFAULT NULL,
  `schedule` varchar(255) NOT NULL DEFAULT '',
  `default_label` varchar(255) NOT NULL DEFAULT '',
  `default_description` text NOT NULL,
  `active_from` datetime DEFAULT NULL,
  `active_to` datetime DEFAULT NULL,
  PRIMARY KEY (`rule_id`),
  KEY `IDX_BRONTO_REMINDER_SALESRULE` (`salesrule_id`),
  CONSTRAINT `FK_BRONTO_REMINDER_SALESRULE` FOREIGN KEY (`salesrule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8