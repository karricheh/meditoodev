CREATE TABLE `udshipclass_vendor_row` (
  `class_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `country_id` varchar(2) NOT NULL,
  `region_id` text NOT NULL,
  `postcode` text,
  UNIQUE KEY `UNQ_CLASS_COUNTRY` (`class_id`,`country_id`),
  KEY `FK_UDSC_VENDOR_CLASS_ID` (`class_id`),
  CONSTRAINT `FK_UDSC_VENDOR_CLASS_ID` FOREIGN KEY (`class_id`) REFERENCES `udshipclass_vendor` (`class_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8