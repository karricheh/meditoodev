CREATE TABLE `usimpleup_module` (
  `module_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) NOT NULL DEFAULT '',
  `download_uri` text NOT NULL,
  `last_checked` datetime DEFAULT NULL,
  `last_downloaded` datetime DEFAULT NULL,
  `last_stability` varchar(30) DEFAULT NULL,
  `last_version` varchar(30) DEFAULT NULL,
  `remote_version` varchar(30) DEFAULT NULL,
  `license_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`module_id`),
  UNIQUE KEY `NewIndex1` (`module_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8