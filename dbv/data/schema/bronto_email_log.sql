CREATE TABLE `bronto_email_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `customer_email` varchar(255) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `message_id` char(36) NOT NULL,
  `message_name` varchar(64) DEFAULT NULL,
  `delivery_id` char(36) DEFAULT NULL,
  `sent_at` datetime DEFAULT NULL,
  `success` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `error` varchar(255) DEFAULT NULL,
  `fields` text,
  PRIMARY KEY (`log_id`),
  KEY `IDX_BRONTO_EMAIL_LOG_CUSTOMER_EMAIL` (`customer_email`),
  KEY `IDX_BRONTO_EMAIL_LOG_SENT_AT` (`sent_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8