CREATE TABLE `themeframework_area` (
  `area_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Area ID',
  `package_theme` varchar(100) DEFAULT NULL COMMENT 'Package Theme',
  `layout` varchar(100) DEFAULT NULL COMMENT 'Layout',
  `content` mediumtext COMMENT 'Area Content',
  `creation_time` timestamp NULL DEFAULT NULL COMMENT 'Area Creation Time',
  `update_time` timestamp NULL DEFAULT NULL COMMENT 'Area Modification Time',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Area Active',
  PRIMARY KEY (`area_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='EM ThemeFramework Area Table'