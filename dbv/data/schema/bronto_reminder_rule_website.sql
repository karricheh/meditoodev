CREATE TABLE `bronto_reminder_rule_website` (
  `rule_id` int(10) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`rule_id`,`website_id`),
  KEY `IDX_BRONTO_REMINDER_WEBSITE` (`website_id`),
  CONSTRAINT `FK_BRONTO_REMINDER_RULE` FOREIGN KEY (`rule_id`) REFERENCES `bronto_reminder_rule` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8