CREATE TABLE `bronto_reminder_rule_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rule_id` int(10) unsigned NOT NULL,
  `unique_id` varchar(20) NOT NULL,
  `sent_at` datetime NOT NULL,
  `bronto_delivery_id` varchar(255) DEFAULT NULL,
  `bronto_message_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `IDX_BRONTO_REMINDER_LOG_RULE` (`rule_id`),
  CONSTRAINT `FK_BRONTO_REMINDER_LOG_RULE` FOREIGN KEY (`rule_id`) REFERENCES `bronto_reminder_rule` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8