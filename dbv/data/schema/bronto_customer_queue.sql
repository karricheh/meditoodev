CREATE TABLE `bronto_customer_queue` (
  `queue_id` int(10) NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL COMMENT 'Customer Entity Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `bronto_imported` datetime DEFAULT NULL COMMENT 'Bronto Imported',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `bronto_suppressed` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`queue_id`,`customer_id`,`store_id`),
  KEY `IDX_BRONTO_CUSTOMER_QUEUE_STORE_ID` (`store_id`),
  KEY `IDX_BRONTO_CUSTOMER_QUEUE_BRONTO_IMPORTED` (`bronto_imported`),
  KEY `IDX_BRONTO_CUSTOMER_QUEUE_CREATED_AT` (`created_at`),
  KEY `IDX_BRONTO_CUSTOMER_QUEUE_UPDATED_AT` (`updated_at`),
  CONSTRAINT `FK_BRONTO_CUSTOMER_QUEUE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bronto Customer Import Queue'