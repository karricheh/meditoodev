CREATE TABLE `udshipclass_customer_row` (
  `class_id` smallint(6) NOT NULL,
  `country_id` varchar(2) NOT NULL,
  `region_id` text NOT NULL,
  `postcode` text,
  UNIQUE KEY `UNQ_CLASS_COUNTRY` (`class_id`,`country_id`),
  KEY `FK_UDSC_CUSTOMER_CLASS_ID` (`class_id`),
  CONSTRAINT `FK_UDSC_CUSTOMER_CLASS_ID` FOREIGN KEY (`class_id`) REFERENCES `udshipclass_customer` (`class_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8