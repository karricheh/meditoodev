CREATE TABLE `bronto_order_queue` (
  `queue_id` int(10) NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Entity Id',
  `quote_id` int(11) unsigned NOT NULL COMMENT 'Quote Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `bronto_tid` varchar(255) DEFAULT NULL COMMENT 'Bronto Tid',
  `bronto_imported` datetime DEFAULT NULL COMMENT 'Bronto Imported',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `bronto_suppressed` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`queue_id`,`order_id`,`store_id`,`quote_id`),
  KEY `IDX_BRONTO_ORDER_QUEUE_STORE_ID` (`store_id`),
  KEY `IDX_BRONTO_ORDER_QUEUE_QUOTE_ID` (`quote_id`),
  KEY `IDX_BRONTO_ORDER_QUEUE_BRONTO_IMPORTED` (`bronto_imported`),
  KEY `IDX_BRONTO_ORDER_QUEUE_CREATED_AT` (`created_at`),
  KEY `IDX_BRONTO_ORDER_QUEUE_UPDATED_AT` (`updated_at`),
  CONSTRAINT `FK_BRONTO_ORDER_QUEUE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bronto Order Import Queue'