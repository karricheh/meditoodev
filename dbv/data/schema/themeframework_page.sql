CREATE TABLE `themeframework_page` (
  `page_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Area ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `handle` varchar(100) DEFAULT NULL COMMENT 'Handle',
  `custom_handle` varchar(100) DEFAULT NULL COMMENT 'Custom Handle',
  `layout` varchar(100) DEFAULT NULL COMMENT 'Layout',
  `layout_update_xml` text COMMENT 'Layout Update Xml',
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Status',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT 'Sort',
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='EM ThemeFramework Page Table'