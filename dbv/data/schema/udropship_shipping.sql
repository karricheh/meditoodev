CREATE TABLE `udropship_shipping` (
  `shipping_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shipping_code` varchar(30) NOT NULL,
  `shipping_title` varchar(100) NOT NULL,
  `days_in_transit` varchar(20) NOT NULL,
  `vendor_ship_class` varchar(255) DEFAULT NULL,
  `customer_ship_class` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`shipping_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8