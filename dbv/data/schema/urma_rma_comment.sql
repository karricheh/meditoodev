CREATE TABLE `urma_rma_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` int(11) DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `is_vendor_notified` tinyint(4) DEFAULT NULL,
  `is_visible_to_vendor` tinyint(4) DEFAULT '1',
  `udropship_status` varchar(64) DEFAULT NULL,
  `username` varchar(40) DEFAULT NULL,
  `rma_status` varchar(128) DEFAULT 'pending',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_SALES_FLAT_SHIPMENT_COMMENT_CREATED_AT` (`created_at`),
  KEY `IDX_SALES_FLAT_SHIPMENT_COMMENT_PARENT_ID` (`parent_id`),
  CONSTRAINT `URMA_RMA_COMMENT_PARENT` FOREIGN KEY (`parent_id`) REFERENCES `urma_rma` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Comment'