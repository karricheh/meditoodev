CREATE TABLE `udropship_po_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL,
  `is_vendor_notified` tinyint(4) DEFAULT NULL,
  `comment` text,
  `created_at` datetime DEFAULT NULL,
  `is_visible_to_vendor` tinyint(4) DEFAULT NULL,
  `udropship_status` varchar(64) DEFAULT NULL,
  `username` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  KEY `IDX_CREATED_AT` (`created_at`),
  KEY `IDX_PARENT_ID` (`parent_id`),
  CONSTRAINT `UDROPSHIP_PO_COMMENT_PARENT` FOREIGN KEY (`parent_id`) REFERENCES `udropship_po` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8