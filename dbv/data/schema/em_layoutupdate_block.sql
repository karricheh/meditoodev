CREATE TABLE `em_layoutupdate_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store_id',
  `area` varchar(255) NOT NULL COMMENT 'Area',
  `package` varchar(255) NOT NULL COMMENT 'Package',
  `theme` varchar(255) NOT NULL COMMENT 'Theme',
  `handle` varchar(255) NOT NULL COMMENT 'Handle',
  `block_name` varchar(255) NOT NULL COMMENT 'Block_name',
  `parent_block` varchar(255) NOT NULL COMMENT 'Parent_block',
  `ordering` smallint(5) unsigned NOT NULL COMMENT 'Ordering',
  `is_hidden` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is_hidden',
  PRIMARY KEY (`id`),
  KEY `IDX_EM_LAYOUTUPDATE_BLOCK_STORE_ID` (`store_id`),
  CONSTRAINT `FK_EM_LAYOUTUPDATE_BLOCK_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='EM LayoutUpdate Saved Block Tables'