CREATE TABLE `bronto_reminder_message` (
  `rule_id` int(10) unsigned NOT NULL,
  `store_id` smallint(5) NOT NULL,
  `message_id` varchar(255) NOT NULL DEFAULT '',
  `label` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`rule_id`,`store_id`),
  KEY `IDX_BRONTO_REMINDER_MESSAGE_RULE` (`rule_id`),
  KEY `IDX_BRONTO_REMINDER_MESSAGE` (`message_id`),
  CONSTRAINT `FK_BRONTO_REMINDER_MESSAGE_RULE` FOREIGN KEY (`rule_id`) REFERENCES `bronto_reminder_rule` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8