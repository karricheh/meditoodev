CREATE TABLE `udropship_label_batch` (
  `batch_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `label_type` varchar(10) NOT NULL DEFAULT 'PDF',
  `created_at` datetime NOT NULL,
  `vendor_id` int(10) unsigned NOT NULL,
  `username` varchar(50) NOT NULL,
  `shipment_cnt` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`batch_id`),
  KEY `vendor_id` (`vendor_id`),
  KEY `created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8